﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Dp.Model;

namespace Dp.Service.User
{
    public class UserServiceImpl : IUserService
    {
        //todo 此处只是模拟从数据库获取
        public IQueryable<Model.User> FilterUser(Expression<Func<Model.User, bool>> exp)
        {
            return exp == null ? Static.UserQuery : Static.UserQuery.Where(exp);
        }
    }
}
