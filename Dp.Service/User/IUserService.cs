﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dp.Service.User
{
    public interface IUserService
    {
        IQueryable<Model.User> FilterUser(Expression<Func<Model.User, bool>> exp);

    }
}
