﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dp.Model
{
    public class Entity
    {
        public Entity()
        {
            Id = Guid.NewGuid();
            CreationTime = DateTime.Now;
        }

        /// <summary>
        /// 主键Id
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }
        /// <summary>
        /// 删除标识
        /// </summary>
        public bool Active { get; set; }
    }
}
