﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dp.Model
{
    /// <summary>
    /// 用户表
    /// </summary>
    public class User : Entity
    {

        /// <summary>
        /// 用户登录名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string PassWord { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
    }

    /// <summary>
    /// 初始化一些模拟数据
    /// </summary>
    public class Static
    {
        public static void Init()
        {
            UserQuery = new List<User>
            {
                new User
                {
                    UserName = "admin",
                    Phone = "158",
                    PassWord="123",
                },
                new User
                {
                    UserName = "abc",
                    Phone = "159",
                    PassWord="123",
                }
            }.AsQueryable();
        }

        public static IQueryable<User> UserQuery { get; set; }
    }
}
