﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security.Infrastructure;

namespace DP.Api.Providers
{
    /// <summary>
    /// 产生Token 的提供器
    /// </summary>
    public class SelfAuthenticationTokenProvider : AuthenticationTokenProvider
    {
        /// <summary>
        /// 创建token时会触发
        /// </summary>
        /// <param name="context"></param>
        public override void Create(AuthenticationTokenCreateContext context)
        {
            //todo用户根据自己的业务产生自己token
            var ticket = context.SerializeTicket();
            context.SetToken(ticket);
        }
    }
}