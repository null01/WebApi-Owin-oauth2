﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;

namespace DP.Api.Providers
{
    public class CentralizedPrefixDirectRouteProvider : DefaultDirectRouteProvider
    {
        private readonly string _centralizedPrefix;

        public CentralizedPrefixDirectRouteProvider(string centralizedPrefix)
        {
            _centralizedPrefix = centralizedPrefix;
        }

        protected override string GetRoutePrefix(HttpControllerDescriptor controllerDescriptor)
        {
            var routePrefix = base.GetRoutePrefix(controllerDescriptor);

            return string.IsNullOrWhiteSpace(routePrefix)
                ? _centralizedPrefix
                : string.Format("{0}/{1}", _centralizedPrefix, routePrefix);
        }
    }
}