﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace DP.Api.Handler
{
    //IExceptionLogger作为异常日志记录组件，负责异常发生后的日志记录，他贯穿于整个Web API的生命周期中，在Web API框架里，任何一个请求周期中出现任何一个未被捕获/处理的异常都会首先进入这个异常日志记录管道进行异常Log记录，在Web API中可以注册多个IExceptionLogger实例负责不同的异常处理。
    public sealed class ApiExceptionLoggerHandler : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            string msgTemplate = "错误时间：" + DateTime.Now.ToString() + ",在执行 {0} 时产生异常\r\n错误详情:{1}" + "\r\n堆栈信息：{2}";
            var request = context.Request;
            string errMsg = string.Format(msgTemplate, request.RequestUri, context.Exception.Message, context.Exception.StackTrace);
            // LogerHelper.WriteLog(DateTime.Now.ToString("yyyyMMdd") + ".txt", "Error", errMsg + "\r\n");//写日志
        }
    }
}