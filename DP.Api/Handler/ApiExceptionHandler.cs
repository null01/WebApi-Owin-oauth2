﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace DP.Api.Handler
{
    //IExceptionHandler作为异常处理组件，负责异常发生后的处理工作，他处于异常处理管道的最末端，当IExceptionLogger组件进行一场记录完毕、没有相关的ExceptoinFilter进行异常处理时，才会最终调用ExceptionHandler进行异常处理，在Web API中，有且仅有一个ExceptionHandler进行异常的处理。
    public sealed class ApiExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            var response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
               "请求出错,请重试");
            context.Result = new ResponseMessageResult(response);

        }
    }
}