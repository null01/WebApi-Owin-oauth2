﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Owin;

namespace DP.Api.App_Start
{
    public static class AutofacConfig
    {
        public static void UseAutofacInjection(this IAppBuilder app)
        {
            var config = GlobalConfiguration.Configuration;
            var currentAssembly = Assembly.GetExecutingAssembly();
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(currentAssembly).PropertiesAutowired().InstancePerLifetimeScope();
            builder.RegisterWebApiFilterProvider(config);

            builder.RegisterAssemblyModules(currentAssembly);
            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
        }
    }
}