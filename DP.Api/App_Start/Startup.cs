﻿using System;
using Dp.Model;
using DP.Api.App_Start;
using DP.Api.Authentication;
using DP.Api.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace DP.Api
{
    public partial class Startup
    {
        public void ConfigureOAuth(IAppBuilder app)
        {
            app.UseAutofacInjection();
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                TokenEndpointPath = new PathString("/api/Authentication/login"),
                Provider = new AppAuthorizationServerProvider(),//提供器（授权实现类）
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(15),
                AllowInsecureHttp = true,

                //以下两处根据自己的需要可拓展
                RefreshTokenProvider = new AppAuthenticationTokenProvider(),//刷新token的提供器
                AccessTokenProvider = new SelfAuthenticationTokenProvider()//产生token的提供器
            };
            app.UseOAuthBearerTokens(OAuthServerOptions);
        }
    }
}