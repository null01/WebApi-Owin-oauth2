﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac;
using Dp.Model;
using Module = Autofac.Module;
namespace DP.Api.App_Start
{

    public class DefaultDependenciesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(Assembly.Load("Dp.Service")).AsImplementedInterfaces();
        }
    }
}