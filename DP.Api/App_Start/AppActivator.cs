﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Dp.Model;
using DP.Api.Filter;
using DP.Api.Handler;
using DP.Api.Providers;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DP.Api.App_Start
{
    public static class AppActivator
    {
        public static void AppPostStart(HttpConfiguration config)
        {
            RegisterApiRoutes(config);
            config.EnsureInitialized();
            ConfigJsonFormatter(config);
            RegisterFilter(config);

            Static.Init();//todo 初始化用户数据(测试用)  
            config.Services.Replace(typeof(IExceptionHandler), new ApiExceptionHandler());
            config.Services.Add(typeof(IExceptionLogger), new ApiExceptionLoggerHandler());

        }
        private static void RegisterApiRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes(new CentralizedPrefixDirectRouteProvider("api"));
            config.Routes.MapHttpRoute(
                name: "Api",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );
        }
        private static void RegisterFilter(HttpConfiguration config)
        {
            config.Filters.Add(new ValidateModelAttribute());//验证请求参数

            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.Filters.Add(new WebApiExceptionFilter());//异常过滤处理
            // config.Formatters.Add(new FormMultipartEncodedMediaTypeFormatter());//多媒体序列化  详细使用方法 搜索关键词:MultipartDataMediaFormatter
        }
        /// <summary>
        /// JsonFormatter配置
        /// </summary>
        private static void ConfigJsonFormatter(HttpConfiguration config)
        {
            var jsonFormatter = config.Formatters.JsonFormatter;
            // jsonp插入格式化器管道
            //config.Formatters.Add(new JsonpMediaTypeFormatter(jsonFormatter));

            jsonFormatter.Indent = false;

            var serializerSettings = jsonFormatter.SerializerSettings;

            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            serializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat; //e.g. "2012-03-21T05:40Z".
            serializerSettings.NullValueHandling = NullValueHandling.Include; // 包含Null值
        }
    }
}