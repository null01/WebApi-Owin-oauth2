﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Controllers;
using Microsoft.AspNet.Identity;

namespace DP.Api.Filter
{
    public class AppAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {

            var response = new HttpResponseMessage { StatusCode = HttpStatusCode.Unauthorized };
            var content = new
            {
                message = "用户还未登录,请先登录"
            };
            response.Content = new StringContent(Json.Encode(content), Encoding.UTF8, "application/json");

            //todo  此处需要优化根据用户类别来判断
            var notLogin = actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();//判断是否有AllowAnonymousAttribute特性，如果存在则允许匿名访问
            if (notLogin)
            {
                return;
            }

            var authorization = actionContext.Request.Headers.Authorization;
            if (authorization == null || actionContext.RequestContext == null || actionContext.RequestContext.Principal == null || actionContext.RequestContext.Principal.Identity == null)
            {
                actionContext.Response = response;
                return;
            }
            var userId = actionContext.RequestContext.Principal.Identity.GetUserId();//获取用户Id
            if (string.IsNullOrWhiteSpace(userId))
            {
                actionContext.Response = response;
                return;
            }
            var tokenStr = authorization.Parameter;//获取到提交的token
            //todo 如有需要可以自己实现token的验证
            if (actionContext.RequestContext.Principal.Identity.IsAuthenticated)//判断用户是否已认证并且验证token的有效性
            {
                base.OnAuthorization(actionContext);//认证通过
                return;
            }
            actionContext.Response = response;
        }
    }
}