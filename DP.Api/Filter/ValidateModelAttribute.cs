﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using DP.Api.Extensions;
using Newtonsoft.Json;

namespace DP.Api.Filter
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid) return;
            var info = actionContext.ModelState.FirstError();
            if (string.IsNullOrWhiteSpace(info)) return;
            var data = new
            {
                Status = false,
                Msg = info
            };
            var response = new HttpResponseMessage
            {
                Content = new StringContent(JsonConvert.SerializeObject(data)),
                StatusCode = HttpStatusCode.OK
            };
            actionContext.Response = response;
            //todo 另一种写法，返回的状态码为400
            //actionContext.Response = actionContext.Request.CreateErrorResponse(
            //HttpStatusCode.BadRequest, actionContext.ModelState.FirstError());
        }
    }
}