﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http.Filters;
using NLog;

namespace DP.Api.Filter
{
    public class WebApiExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            string path = HostingEnvironment.MapPath("~/") + "error\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            if (actionExecutedContext.Exception != null)
            {
                //todo log4的日志信息目前无效
                LogManager.GetLogger("error")
                    .Error(actionExecutedContext.Exception.Message, actionExecutedContext.Exception);
                string msgTemplate = "错误时间：" + DateTime.Now.ToString() + ",在执行 {0} 时产生异常\r\n错误详情:{1}" + "\r\n堆栈信息：{2}";
                string errMsg = string.Format(msgTemplate, actionExecutedContext.ActionContext.ControllerContext.RouteData.Route.RouteTemplate, actionExecutedContext.Exception.GetBaseException().Message, actionExecutedContext.Exception.StackTrace);
                // LogerHelper.WriteLog(DateTime.Now.ToString("yyyyMMdd") + ".txt", "Error", errMsg + "\r\n");//日志信息记录
            }
            var response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
               "请求出错,请重试");
            actionExecutedContext.Response = response;
            base.OnException(actionExecutedContext);
        }
    }
}