﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Autofac;
using Autofac.Integration.Owin;
using Dp.Service.User;
using Microsoft.Owin;

namespace DP.Api.Extensions
{
    public static class OwinContextExtensions
    {
        public static string GetClaimOrDefault(this ClaimsPrincipal self, string claimType)
        {
            if (self == null)
                throw new ArgumentNullException("self");
            return self.Claims.GetClaimOrDefault(claimType);
        }

        public static string GetClaimOrDefault(this ClaimsIdentity self, string claimType)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            return self.Claims.GetClaimOrDefault(claimType);
        }

        public static string GetClaimOrDefault(this IEnumerable<Claim> self, string claimType)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            return self
                .Where(c => String.Equals(c.Type, claimType, StringComparison.OrdinalIgnoreCase))
                .Select(c => c.Value)
                .FirstOrDefault();
        }

        public static bool IsAuthenticated(this ClaimsPrincipal self)
        {
            return self != null && self.Identity != null && self.Identity.IsAuthenticated;
        }

        public static string GetUserName(this ClaimsPrincipal self)
        {
            return self.GetClaimOrDefault(ClaimTypes.Name);
        }


        public static string GetUserPhoneNumber(this ClaimsPrincipal self)
        {
            return self.GetClaimOrDefault(ClaimTypes.MobilePhone);
        }

        public static string GetUserId(this ClaimsPrincipal self)
        {
            return self.GetClaimOrDefault(ClaimTypes.NameIdentifier);
        }


        public static Dp.Model.User GetCurrentUser(this IOwinContext context)
        {
            if (context.Request.User == null)
                return null;

            Dp.Model.User user;

            if (context.Environment.TryGetValue("user", out user))
                return user;

            user = LoadUser(context);

            if (user == null)
                throw new HttpException(401, "用户未登录,请先登录");
            return user;
        }

        private static Dp.Model.User LoadUser(IOwinContext context)
        {
            var principal = context.Authentication.User;
            if (principal != null)
            {
                var userId = principal.GetUserId();
                if (!string.IsNullOrWhiteSpace(userId))
                {
                    var userIdG = Guid.Parse(userId);
                    return context.GetAutofacLifetimeScope()
                        .Resolve<IUserService>()
                        .FilterUser(o => o.Id.Equals(userIdG)).FirstOrDefault();
                }
                var phoneNumber = principal.GetUserPhoneNumber();

                if (!string.IsNullOrEmpty(phoneNumber))
                {
                    return context.GetAutofacLifetimeScope()
                        .Resolve<IUserService>().FilterUser(o => o.Phone.Equals(phoneNumber)).FirstOrDefault();
                }
            }
            return null;
        }
    }
}