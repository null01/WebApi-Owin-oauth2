﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

namespace DP.Api.Extensions
{
    public static class DictionaryExtensions
    {
        public static bool TryGetValue<TValue>(this IDictionary<string, object> collection, string key, out TValue value)
        {
            if (collection == null)
                throw new ArgumentNullException("collection");

            object obj;
            if (collection.TryGetValue(key, out obj))
            {
                if (obj is TValue)
                {
                    value = (TValue)obj;
                    return true;
                }
            }
            value = default(TValue);
            return false;
        }

        public static string FirstError(this IDictionary<string, ModelState> modelState)
        {
            if (modelState == null) throw new ArgumentNullException("modelState");

            return (from modelstate in modelState
                    where modelstate.Value.Errors.Count > 0
                    from error in modelstate.Value.Errors
                    select error.ErrorMessage).First();
        }
    }
}