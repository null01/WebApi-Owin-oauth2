﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DP.Api.Extensions;
using Microsoft.Owin;

namespace DP.Api.Controllers
{
    public class BaseController : ApiController
    {
        private IOwinContext _owinContext;
        public IOwinContext OwinContext
        {
            get { return _owinContext ?? (_owinContext = Request.GetOwinContext()); }
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        protected Dp.Model.User GetCurrentUser()
        {
            return OwinContext.GetCurrentUser();
        }

    }
}
