﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DP.Api.Controllers
{
    /// <summary>
    /// 首页
    /// </summary>
    public class IndexController : Controller
    {
        //
        // GET: /Index/
        public ActionResult Index()
        {
            //跳转到帮助页////////
            return Redirect(Request.Url.AbsoluteUri + "document");
        }
    }
}