﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dp.Service.User;
using DP.Api.Filter;

namespace DP.Api.Controllers
{
    /// <summary>
    /// 首页面
    /// </summary>
    [AppAuthorize]
    [RoutePrefix("Home")]
    public class HomeController : BaseController
    {
        private readonly IUserService _userService;
        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// 获取所有用户信息,不需要登录
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("AllUser"), HttpGet]
        public IHttpActionResult Index()
        {
            return Ok(_userService.FilterUser(null));
        }

        /// <summary>
        /// 获取自己的个人信息,需要登录
        /// </summary>
        /// <returns></returns>
        [Route("MyInfo"), HttpGet]
        public IHttpActionResult GetMyInfo()
        {
            return Ok(base.GetCurrentUser());
        }
    }
}
