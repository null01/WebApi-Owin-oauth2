﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Dp.Model;

namespace DP.Api.Authentication
{
    public class ClaimsIdentityImpl : IClaimsIdentity
    {
        private readonly IGenerateClaimsIdentityFactory<User> _IdentityFactory;

        public ClaimsIdentityImpl(IGenerateClaimsIdentityFactory<User> IdentityFactory)
        {
            _IdentityFactory = IdentityFactory;
        }

        public ClaimsIdentity GenerateUserClaimsIdentity(User user, string authenticationType)
        {
            return _IdentityFactory.Create(user, authenticationType);
        }
    }
}