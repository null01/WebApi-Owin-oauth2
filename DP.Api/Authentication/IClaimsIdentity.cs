﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Dp.Model;

namespace DP.Api.Authentication
{
    interface IClaimsIdentity
    {
        /// <summary>
        /// 创建基于声明的标识
        /// </summary>
        /// <param name="user">已验证的用户</param>
        /// <param name="authenticationType">登录类型</param>
        /// <returns>基于声明的标识</returns>
        ClaimsIdentity GenerateUserClaimsIdentity(User user, string authenticationType);
    }
}
