﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Dp.Model;

namespace DP.Api.Authentication
{
    public interface IGenerateClaimsIdentityFactory<in T> where T : Entity
    {
        ClaimsIdentity Create(T user, string authenticationType);
    }
    public class UserClaimsIdentityFactory : IGenerateClaimsIdentityFactory<User>
    {
        public ClaimsIdentity Create(User user, string authenticationType)
        {
            var claims = new List<Claim>
            {
                //todo 此处可以存入任意信息
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString(""), ClaimValueTypes.String),
                new Claim(ClaimTypes.MobilePhone, user.Phone, ClaimValueTypes.String),
                new Claim(ClaimTypes.Name, user.UserName, ClaimValueTypes.String),
            };
            //todo 此处还可以存入其他信息
            //if (!string.IsNullOrWhiteSpace(user.LoginName))
            //{
            //    claims.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, user.LoginName, ClaimValueTypes.String));
            //}

            //if (!string.IsNullOrWhiteSpace(user.Mobel))
            //{
            //    claims.Add(new Claim(ClaimTypes.MobilePhone, user.Mobel));
            //}

            var identity = new ClaimsIdentity(claims.AsEnumerable(), authenticationType, ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            return identity;
        }
    }
}
