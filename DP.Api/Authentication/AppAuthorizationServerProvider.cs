﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Autofac;
using Autofac.Integration.Owin;
using Dp.Service.User;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace DP.Api.Authentication
{
    public class AppAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// 在对令牌终结点的请求到达时“grant_type”为“password”的情况下调用。 在用户已将名称和密码凭据直接提供到客户端应用程序的用户界面中，而客户端应用程序正在使用这些凭据获取“access_token”和可选的“refresh_token”时，将发生这种情况。 如果 Web 应用程序支持资源所有者凭据授权类型，则它必须根据情况验证 context.Username 和 context.Password。 若要颁发访问令牌，调用 context.Validated 时必须使用新票证，其中包含应与访问令牌关联的资源所有者的相关声明。 默认行为是拒绝此授权类型。 另请参阅 http://tools.ietf.org/html/rfc6749#section-4.3.2
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //todo 默认登录方式为用户名密码
            var userName = context.UserName;
            var passWord = context.Password;
            var scope = context.OwinContext.GetAutofacLifetimeScope();
            var service = scope.Resolve<IUserService>();
            var user = service.FilterUser(o => o.UserName.Equals(userName) && o.PassWord.Equals(passWord)).FirstOrDefault();
            if (user == null)
            {
                context.SetError("message", "用户名或密码错误");
                return;
            }
            //下面需要带出用户其他信息
            context.OwinContext.Set("user", user);
            var authentication = scope.Resolve<IClaimsIdentity>();
            var identity = authentication.GenerateUserClaimsIdentity(user, OAuthDefaults.AuthenticationType);
            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());

            context.Validated(ticket);
        }

        /// <summary>
        /// 自定义登录方式
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task GrantCustomExtension(OAuthGrantCustomExtensionContext context)
        {
            if (context.GrantType.Equals("phone", StringComparison.OrdinalIgnoreCase)) //手机号登录
            {
                await GrantResourceOwnerCredentials(context, "phone");
            }
        }

        /// <summary>
        /// 自定义登录方式实现
        /// </summary>
        /// <param name="context"></param>
        /// <param name="loginType"></param>
        /// <returns></returns>
        private async Task GrantResourceOwnerCredentials(OAuthGrantCustomExtensionContext context,
           string loginType)
        {
            //todo 此处默认登录方式为phone
            Func<string, string> getParameter = context.Parameters.Get;
            var phone = getParameter("phone");
            var passWord = getParameter("password");
            var scope = context.OwinContext.GetAutofacLifetimeScope();
            var service = scope.Resolve<IUserService>();
            var user = service.FilterUser(o => o.Phone.Equals(phone) && o.PassWord.Equals(passWord)).FirstOrDefault();
            if (user == null)
            {
                context.SetError("message", "用户名或密码错误");
                return;
            }
            //下面需要带出用户其他信息
            context.OwinContext.Set("user", user);
            var authentication = scope.Resolve<IClaimsIdentity>();
            var identity = authentication.GenerateUserClaimsIdentity(user, OAuthDefaults.AuthenticationType);
            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());

            context.Validated(ticket);
        }


        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            //todo 此处可以get上面传递的对象
            var user = context.OwinContext.Get<Dp.Model.User>("user");
            //可以在返回值中加入其他字段
            if (user != null) context.AdditionalResponseParameters.Add("id", user.Id);
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }
            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// 刷新token
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var newId = new ClaimsIdentity(context.Ticket.Identity);

            var newTicket = new AuthenticationTicket(newId, context.Ticket.Properties);
            context.Validated(newTicket);

            await base.GrantRefreshToken(context);
        }
    }
}